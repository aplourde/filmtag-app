![FilmTag App Logo](http://filmtagapp.com/images/icon.png)

## Automagically adds metadata to your movies
FilmTag automatically adds metadata information to iTunes compatible movie files (mp4 or mv4). It uses my mp4v2-cocoa framework to process video files and theMovieDB to fetch movies info.

## Step 1 : Drag & Drop
Drag an iTunes compatible movie file into FilmTag

![drag](http://filmtagapp.com/images/screenshots/drag.png)

## Step 2 : Identification
FilmTag will automatically identity the movie from its file name.

![drag](http://filmtagapp.com/images/screenshots/confirm.png)

If for wathever reason, the identification failed. You can always search by yourself the corresponing movie.

![drag](http://filmtagapp.com/images/screenshots/search.png)

## Step 3 : Process
Once you have confirm the identification of the movie, FilmTag will process the movie by full filling its metadata (title, synopsis, release year, artwork, etc).

![drag](http://filmtagapp.com/images/screenshots/process.png)

## Step 4 : Enjoy a beautiful movie library
![drag](http://filmtagapp.com/images/screenshots/enjoy2.png)
![drag](http://filmtagapp.com/images/screenshots/enjoy1.png)
