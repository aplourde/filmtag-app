//
//  Constants.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-20.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#ifndef FilmTag_Constants_h
#define FilmTag_Constants_h

// AdmitOne API infos

//#define kAdmitOneApiEndPoint                @"http://localhost:8100/api"
#define kAdmitOneApiEndPoint                @"http://filmtagapi-aplourde.rhcloud.com/api"
#define kAdmitOneApiResourceResolve         @"/file/resolve?fileName=%@&folder=%@&parentFolder=%@&filmtagVersion=%@"
#define kAdmitOneApiResourceSearch          @"/file/search?keywords=%@&filmtagVersion=%@"
#define kAdmitOneApiResourceHealth          @"/health"

// TheMovieDB API infos

#define kTheMovieDBApiEndPoint                @"http://api.themoviedb.org/3"
#define kTheMovieDBApiResourceMovie           @"/movie/%ld?api_key=5c7eb72a118791e6964844c84a6bea49&append_to_response=casts,releases&language=%@"
#define kTheMovieDBApiResourceTvShow          @"/tv/%ld?api_key=5c7eb72a118791e6964844c84a6bea49&language=%@&append_to_response=content_ratings"
#define kTheMovieDBApiResourceTvShowEpisode   @"/tv/%ld/season/%ld/episode/%ld?api_key=5c7eb72a118791e6964844c84a6bea49&append_to_response=credits&language=%@"
#define kTheMovieDBImagesUrl                  @"http://image.tmdb.org/t/p/w396"

// User Defaults keys

#define kAPPreferencesPreferredLanguage       @"preferredLanguage"
#define kAPPreferencesPreferredCountry        @"preferredCountry"


#endif
