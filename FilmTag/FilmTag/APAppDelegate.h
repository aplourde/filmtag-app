//
//  APAppDelegate.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-20.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <GAJavaScriptTracker/GAJavaScriptTracker.h>

@class APPreferencesWindowController;
@class APQueueWindowController;

@interface APAppDelegate : NSObject <NSApplicationDelegate> {
    
    NSMutableArray *_mainWindows;
    GAJavaScriptTracker *_tracker;
    APPreferencesWindowController *_preferencesWindowController;
    APQueueWindowController *_queueWindowController;
}

- (IBAction)newWindow:(id)sender;
- (IBAction)queue:(id)sender;
- (NSWindow*)freeWindow;
- (BOOL)freeWindowAvailable;

@end
