//
//  main.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-20.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
