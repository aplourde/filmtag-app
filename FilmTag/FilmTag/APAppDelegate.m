//
//  APAppDelegate.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-20.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APAppDelegate.h"
#import "APMainWindowController.h"
#import "APPreferencesWindowController.h"
#import "APQueueWindowController.h"
#import "Constants.h"
#import "APHTTPRequestServiceSupport.h"

@implementation APAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _mainWindows = [[NSMutableArray alloc] init];
    [self newWindow:nil];
#ifndef DEBUG
    [self _startGoogleAnalytics];
#endif
    [self _initializePreferences];
    [self _checkServerHealth];
}

- (void)applicationWillTerminate:(NSNotification *)notification {
#ifndef DEBUG
    [self _stopGoogleAnalytics];
#endif
}

- (IBAction)newWindow:(id)sender {

    APMainWindowController *newWindowCtrl = [[APMainWindowController alloc] initWithWindowNibName:@"APMainWindow"];
    [_mainWindows addObject:newWindowCtrl];
    [[newWindowCtrl window] setBackgroundColor:NSColor.whiteColor];
    [[newWindowCtrl window] makeKeyAndOrderFront:nil];

    [[NSNotificationCenter defaultCenter] addObserverForName:NSWindowWillCloseNotification object:[newWindowCtrl window]
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [[NSNotificationCenter defaultCenter] removeObserver:self name:NSWindowWillCloseNotification object:[note object]];
                                                      [_mainWindows removeObject:[[note object] windowController]];
                                                      [[[note object] windowController] close];
                                                  }];
}

- (IBAction)open:(id)sender {

    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseFiles:YES];
    [openPanel setAllowedFileTypes:[NSArray arrayWithObjects:@"mp4", @"m4v", @"mov", nil]];

    if (![self freeWindowAvailable]) {
        [self newWindow:nil];
    }

    NSWindow *keyWindow = [self freeWindow];
    [keyWindow makeKeyAndOrderFront:nil];

    [openPanel beginSheetModalForWindow:keyWindow completionHandler:^(NSInteger result) {
        if (result == NSOKButton) {
            NSArray *files = [NSArray arrayWithObject:[[openPanel URL] path]];
            if ([files count] > 0) {
                [[keyWindow windowController] fileDropped:files];

            }
        }
    }];

}

- (IBAction)preferences:(id)sender {

    if (_preferencesWindowController == nil) {
        _preferencesWindowController = [[APPreferencesWindowController alloc] initWithWindowNibName:@"APPreferencesWindow"];
    }

    [[_preferencesWindowController window] makeKeyAndOrderFront:nil];

}

- (IBAction)queue:(id)sender {

    if (_queueWindowController == nil) {
        _queueWindowController = [APQueueWindowController sharedWindowController];
    }

    [[_queueWindowController window] makeKeyAndOrderFront:nil];
}

- (NSWindow *)freeWindow {
    for (APMainWindowController *windowCtrl in _mainWindows) {
        if ([windowCtrl currentFilePath] == nil) {
            return [windowCtrl window];
        }
    }
    return nil;
}

- (BOOL)freeWindowAvailable {
    return [self freeWindow] != nil;
}

- (void)_startGoogleAnalytics {

    _tracker = [GAJavaScriptTracker trackerWithAccountID:@"UA-41699707-1"];
    if (!_tracker.isRunning) {
        [_tracker start];
    }
    [_tracker trackEvent:@"FilmTag Mac App" action:@"launched" label:@"FilmTag Mac App Launched" value:-1 withError:nil];
}

- (void)_stopGoogleAnalytics {
    if (_tracker.isRunning) {
        [_tracker stop];
    }
}

- (void)_initializePreferences {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:kAPPreferencesPreferredLanguage] == nil) {
        NSString *userPreferredLanguage = [[defaults objectForKey:@"AppleLanguages"] objectAtIndex:0];
        [defaults setObject:userPreferredLanguage forKey:kAPPreferencesPreferredLanguage];
    }
    if ([defaults objectForKey:kAPPreferencesPreferredCountry] == nil) {
        [defaults setObject:@"us" forKey:kAPPreferencesPreferredCountry];
    }
    [defaults synchronize];
}

- (void)_checkServerHealth {
    APHTTPRequestServiceSupport *httpRequester = [[APHTTPRequestServiceSupport alloc] init];
    NSURL *healthUrl = [NSURL URLWithString:[kAdmitOneApiEndPoint stringByAppendingFormat:kAdmitOneApiResourceHealth]];
    [httpRequester performActionRequestToURL:healthUrl usingCache:NO withMethod:@"GET" body:nil];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {

    if (!flag) {
        [self newWindow:nil];
    }
    return !flag;
}

- (void)application:(NSApplication *)sender openFiles:(NSArray *)filenames {

    if (![self freeWindowAvailable]) {
        [self newWindow:nil];
    }

    NSWindow *keyWindow = [self freeWindow];
    [keyWindow makeKeyAndOrderFront:nil];

    [[keyWindow windowController] fileDropped:filenames];
}


@end
