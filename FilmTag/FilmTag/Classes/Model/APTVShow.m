//
//  APTVShow.m
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APTVShow.h"
#import "NSDictionary+APSafeDictionary.h"
#import "Constants.h"


@implementation APTVShow

@synthesize episodeNumber = _episodeNumber;
@synthesize seasonNumber = _seasonNumber;
@synthesize episodeId = _episodeId;
@synthesize seriesTitle = _seriesTitle;
@synthesize seriesId = _seriesId;


- (id)initWithTheMovieDBDictionary:(NSDictionary *)dict
{

    if ((self = [super init]))
    {

        self.episodeNumber = [dict safeIntegerForKey:@"episode_number"];
        self.seasonNumber = [dict safeIntegerForKey:@"season_number"];
        self.episodeId = [NSString stringWithFormat:@"%i", [dict safeIntegerForKey:@"id"]];
        self.synopsis = [dict safeStringForKey:@"overview"];
        self.title = [dict safeStringForKey:@"name"];
        self.year = [[[[dict safeStringForKey:@"air_date"] componentsSeparatedByString:@"-"] objectAtIndex:0] integerValue];
        self.contentId = [dict safeIntegerForKey:@"id"];

        self.cast = [[NSMutableArray alloc] init];
        self.directors = [[NSMutableArray alloc] init];
        self.producers = [[NSMutableArray alloc] init];
        self.screenwriters = [[NSMutableArray alloc] init];

        if ([dict objectForKeyNotNull:@"credits"])
        {
            NSDictionary *castAndCrew = [dict objectForKey:@"credits"];
            for (NSDictionary *actor in [castAndCrew safeArrayForKey:@"cast"])
            {
                if ([actor objectForKeyNotNull:@"name"])
                {
                    [self.cast addObject:[actor safeStringForKey:@"name"]];
                }
            }
            for (NSDictionary *actor in [castAndCrew safeArrayForKey:@"guest_stars"])
            {
                if ([actor objectForKeyNotNull:@"name"])
                {
                    [self.cast addObject:[actor safeStringForKey:@"name"]];
                }
            }
            if ([[castAndCrew safeArrayForKey:@"crew"] count] > 0)
            {
                for (NSDictionary *crewPerson in [castAndCrew safeArrayForKey:@"crew"])
                {
                    if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Director"])
                    {
                        if ([crewPerson objectForKeyNotNull:@"name"])
                        {
                            [self.directors addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    }
                    else if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Writer"])
                    {
                        if ([crewPerson objectForKeyNotNull:@"name"])
                        {
                            [self.screenwriters addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    }
                    else if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Producer"])
                    {
                        if ([crewPerson objectForKeyNotNull:@"name"])
                        {
                            [self.producers addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    }
                }
            }
        }
    }
    return self;
}


- (NSString *)searchQuery
{
    return self.description;
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ s%02lde%02ld", self.seriesTitle, self.seasonNumber, self.episodeNumber];
}


- (void)completeWithTheMovieDBSeriesDict:(NSDictionary *)dict
{

    self.seriesId = [NSString stringWithFormat:@"%i", [dict safeIntegerForKey:@"id"]];
    self.seriesTitle = [dict safeStringForKey:@"name"];
    if ([dict safeArrayForKey:@"genres"].count > 0)
    {
        self.genre = [[[dict safeArrayForKey:@"genres"] objectAtIndex:0] safeStringForKey:@"name"];
    }
    if ([dict safeArrayForKey:@"networks"].count > 0)
    {
        self.studio = [[[dict safeArrayForKey:@"networks"] objectAtIndex:0] safeStringForKey:@"name"];
    }
    for (NSDictionary *season in [dict safeArrayForKey:@"seasons"])
    {
        if ([season safeIntegerForKey:@"season_number"] == self.seasonNumber)
        {
            self.imageURL = [NSURL URLWithString:[kTheMovieDBImagesUrl stringByAppendingString:[season safeStringForKey:@"poster_path"]]];
        }
    }
    self.contentRating = [self contentRatingFromDict:dict];
}


- (NSString *)contentRatingFromDict:(NSDictionary *)dict
{
    NSString *rating = nil;
    if ([dict objectForKeyNotNull:@"content_ratings"])
    {
        NSDictionary *contentRatings = [dict objectForKey:@"content_ratings"];
        NSString *preferredCountry = [[NSUserDefaults standardUserDefaults] stringForKey:kAPPreferencesPreferredCountry];
        for (NSDictionary *contentRating in [contentRatings safeArrayForKey:@"results"])
        {
            NSString *country = [[contentRating safeStringForKey:@"iso_3166_1"] lowercaseString];
            if ([country isEqualToString:preferredCountry] || [country isEqualTo:@"us"])
            {
                NSString *certification = [contentRating safeStringForKey:@"rating"];
                NSString *compatibleCertification = [self iTunesCertificationForCountry:country andCode:certification];
                if (compatibleCertification)
                {
                    rating = compatibleCertification;
                    if ([country isEqualToString:preferredCountry])
                    {
                        break;
                    }
                }
            }
        }
    }
    return rating;
}


- (NSDictionary *)availableContentRatings
{
    NSDictionary *mainRatings = [super availableContentRatings];
    NSMutableDictionary *tvShowsRating = [NSMutableDictionary dictionaryWithDictionary:@{
        @"fr" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"10" : @"fr-tv|-10|100|",
            @"12" : @"fr-tv|-12|200|",
            @"16" : @"fr-tv|-16|500|",
            @"18" : @"fr-tv|-18|600|",
        }],
        @"au" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"P" : @"au-tv|P|100|",
            @"C" : @"au-tv|C|200|",
            @"G" : @"au-tv|G|300|",
            @"PG" : @"au-tv|PG|400|",
            @"M" : @"au-tv|M|500|",
            @"MA15+" : @"au-tv|MA15+|550|",
            @"AV15+" : @"au-tv|AV15+|575|",
            @"R18+" : @"au-tv|R18+|600|",
        }],
        @"gb" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"U" : @"uk-movie|U|100|",
            @"PG" : @"uk-movie|PG|200|",
            @"12A" : @"uk-movie|12A|325|",
            @"12" : @"uk-movie|12|300|",
            @"15" : @"uk-movie|15|350|",
            @"18" : @"uk-movie|18|400|",
            @"R18" : @"uk-movie|R18|600|",
        }],
        @"ca" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"Exempt" : @"ca-tv|TV-E|000|",
            @"C" : @"ca-tv|TV-C|50|",
            @"C8" : @"ca-tv|TV-C8|75|",
            @"G" : @"ca-tv|TV-G|100|",
            @"PG" : @"ca-tv|TV-PG|200|",
            @"14+" : @"ca-tv|TV-14+|300|",
            @"18+" : @"ca-tv|TV-18+|350|",
            @"21+" : @"ca-tv|TV-21+|500|",
        }],
        @"de" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"0" : @"de-tv|ab 0 Jahren|75|",
            @"6" : @"de-tv|ab 6 Jahren|100|",
            @"12" : @"de-tv|ab 12 Jahren|200|",
            @"16" : @"de-tv|ab 16 Jahren|500|",
            @"18" : @"de-tv|ab 18 Jahren|600|",
        }],
        @"us" : [NSMutableDictionary dictionaryWithDictionary:@{
            @"NR" : @"us-tv|Unrated|???|",
            @"TV-Y" : @"us-tv|TV-Y|100|",
            @"TV-Y7" : @"us-tv|TV-Y7|200|",
            @"TV-G" : @"us-tv|TV-G|300|",
            @"TV-PG" : @"us-tv|TV-PG|400|",
            @"TV-14" : @"us-tv|TV-14|500|",
            @"TV-MA" : @"us-tv|TV-MA|600|",
        }]
    }];
    for (NSString *countryCode in [tvShowsRating allKeys])
    {
        NSDictionary *mainCountryRatings = [mainRatings objectForKey:countryCode];
        NSMutableDictionary *tvShowCountryRatings = [tvShowsRating objectForKey:countryCode];
        for (NSString *rating in mainCountryRatings)
        {
            if (![tvShowCountryRatings objectForKey:rating])
            {
                [tvShowCountryRatings setObject:[mainCountryRatings objectForKey:rating] forKey:rating];
            }
        }
    }
    return tvShowsRating;
}
@end
