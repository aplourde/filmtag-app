//
//  APVideoMedia.h
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APVideoMedia : NSObject {
@private
    NSInteger _contentId;
    NSString *_title;
    NSURL *_imageURL;
    NSInteger _year;
    NSString *_synopsis;
    NSString *_genre;
    NSString *_contentRating;
    NSMutableArray *_cast;
    NSMutableArray *_directors;
    NSMutableArray *_producers;
    NSMutableArray *_screenwriters;
    NSString *_studio;
    BOOL _isHD;
    BOOL _isWidescreen;
}
@property(nonatomic) NSInteger contentId;
@property(nonatomic, retain) NSString *title;
@property(assign) NSInteger year;
@property(nonatomic, retain) NSURL *imageURL;
@property(nonatomic, retain) NSString *synopsis;
@property(nonatomic, retain) NSString *genre;
@property(nonatomic, retain) NSString *contentRating;
@property(nonatomic, retain) NSMutableArray *cast;
@property(nonatomic, retain) NSMutableArray *directors;
@property(nonatomic, retain) NSMutableArray *producers;
@property(nonatomic, retain) NSMutableArray *screenwriters;
@property(nonatomic, retain) NSString *studio;
@property(assign) BOOL isHD;
@property(assign) BOOL isWidescreen;

- (NSDictionary *)availableContentRatings;

- (NSString *)iTunesCertificationForCountry:(NSString *)country andCode:(NSString *)code;
- (NSString *)searchQuery;

@end
