//
//  APQueueItem.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APQueueItem.h"

@implementation APQueueItem

@synthesize medias = _medias;
@synthesize filePath = _filePath;
@synthesize currentIndex = _currentIndex;
@synthesize progress = _progress;
@synthesize status = _status;
@synthesize statusMessage = _statusMessage;

- (id)init {

    _medias = [[NSMutableArray alloc] init];
    return self;
}

- (NSImage *)image {

    if (self.currentMedia != nil && self.currentMedia.imageURL != nil) {
        _image = [[NSImage alloc] initWithContentsOfURL:[self.currentMedia imageURL]];
    }
    return _image;
}

- (APVideoMedia *)currentMedia {

    if (_medias == nil || [_medias count] < 1 || _currentIndex < 0) {
        return nil;
    }

    return [_medias objectAtIndex:_currentIndex];
}

@end
