//
//  APMovie.m
//  AdmitOne
//
//  Created by Anthony Plourde on 12-01-06.
//  Copyright (c) 2013 Anthony Plourde.
//

#import "APMovie.h"
#import "NSDictionary+APSafeDictionary.h"
#import "Constants.h"

@implementation APMovie

@synthesize imdbId = _imdbId;


- (id)initWithTheMovieDBDictionary:(NSDictionary *)dict {

    if ((self = [super init])) {

        self.contentId = [dict safeIntegerForKey:@"id"];
        self.imdbId = [dict safeStringForKey:@"imdb_id"];
        self.title = [dict safeStringForKey:@"title"];
        self.year = [[[[dict safeStringForKey:@"release_date"] componentsSeparatedByString:@"-"] objectAtIndex:0] integerValue];
        self.synopsis = [dict safeStringForKey:@"overview"];

        if ([[dict safeArrayForKey:@"genres"] count] > 0) {
            self.genre = [[[dict safeArrayForKey:@"genres"] objectAtIndex:0] safeStringForKey:@"name"];
        }
        if ([dict objectForKeyNotNull:@"poster_path"]) {
            self.imageURL = [NSURL URLWithString:[kTheMovieDBImagesUrl stringByAppendingString:[dict objectForKey:@"poster_path"]]];
        }

        if ([[dict safeArrayForKey:@"production_companies"] count] > 0) {
            self.studio = [[[dict safeArrayForKey:@"production_companies"] objectAtIndex:0] safeStringForKey:@"name"];
        }

        if ([[dict safeArrayForKey:@"production_companies"] count] > 0) {
            self.studio = [[[dict safeArrayForKey:@"production_companies"] objectAtIndex:0] safeStringForKey:@"name"];
        }

        self.contentRating = [self contentRatingFromDict:dict];
        self.cast = [[NSMutableArray alloc] init];
        self.directors = [[NSMutableArray alloc] init];
        self.producers = [[NSMutableArray alloc] init];
        self.screenwriters = [[NSMutableArray alloc] init];

        if ([dict objectForKeyNotNull:@"casts"]) {
            NSDictionary *castAndCrew = [dict objectForKey:@"casts"];
            for (NSDictionary *actor in [castAndCrew safeArrayForKey:@"cast"]) {
                if ([actor objectForKeyNotNull:@"name"]) {
                    [self.cast addObject:[actor safeStringForKey:@"name"]];
                }
            }
            if ([[castAndCrew safeArrayForKey:@"crew"] count] > 0) {
                for (NSDictionary *crewPerson in [castAndCrew safeArrayForKey:@"crew"]) {
                    if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Director"]) {
                        if ([crewPerson objectForKeyNotNull:@"name"]) {
                            [self.directors addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    } else if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Writer"]) {
                        if ([crewPerson objectForKeyNotNull:@"name"]) {
                            [self.screenwriters addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    } else if ([[crewPerson safeStringForKey:@"job"] isEqualTo:@"Producer"]) {
                        if ([crewPerson objectForKeyNotNull:@"name"]) {
                            [self.producers addObject:[crewPerson safeStringForKey:@"name"]];
                        }
                    }
                }
            }
        }
    }
    return self;
}

- (NSString *)contentRatingFromDict:(NSDictionary *)dict
{
    NSString *rating = nil;
    if ([dict objectForKeyNotNull:@"releases"]) {
        NSString *preferredCountry = [[NSUserDefaults standardUserDefaults] stringForKey:kAPPreferencesPreferredCountry];
        for (NSDictionary *release in [[dict objectForKey:@"releases"] safeArrayForKey:@"countries"]) {
            NSString *country = [[release safeStringForKey:@"iso_3166_1"] lowercaseString];
            if ([country isEqualToString:preferredCountry] || [country isEqualTo:@"us"]) {
                NSString *certification = [release safeStringForKey:@"certification"];
                NSString *compatibleCertification = [self iTunesCertificationForCountry:country andCode:certification];
                if (compatibleCertification)
                {
                    rating = compatibleCertification;
                    if ([country isEqualToString:preferredCountry])
                    {
                        break;
                    }
                }
            }
        }
    }
    return rating;
}


- (NSString *)description {

    if (self.year > 0) {
        return [NSString stringWithFormat:@"%@ (%ld)", self.title, self.year];
    } else {
        return self.title;
    }
}

@end
