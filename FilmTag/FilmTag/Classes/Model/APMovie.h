//
//  APMovie.h
//
//  Created by Anthony Plourde on 12-01-06.
//  Copyright (c) 2013 Anthony Plourde.
//
//

#import <Foundation/Foundation.h>
#import "APVideoMedia.h"

@interface APMovie : APVideoMedia {
@private
    NSString *_imdbId;
}

- (id)initWithTheMovieDBDictionary:(NSDictionary *)dict;

@property(nonatomic, retain) NSString *imdbId;

@end
