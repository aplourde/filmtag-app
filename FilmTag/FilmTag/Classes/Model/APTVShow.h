//
//  APTVShow.h
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APVideoMedia.h"

@interface APTVShow : APVideoMedia {
@private
    NSInteger _seasonNumber;
    NSInteger _episodeNumber;
    NSString *_episodeId;
    NSString *_seriesTitle;
    NSString *_seriesId;
}
@property(assign) NSInteger seasonNumber;
@property(assign) NSInteger episodeNumber;
@property(nonatomic, retain) NSString *episodeId;
@property(nonatomic, retain) NSString *seriesTitle;
@property(nonatomic, retain) NSString *seriesId;

- (id)initWithTheTVDBEpisodeDict:(NSDictionary *)dict;

- (void)completeWithTheTVDBSeriesDict:(NSDictionary *)dict;

- (id)initWithTheMovieDBDictionary:(NSDictionary *)dictionary;

- (void)completeWithTheMovieDBSeriesDict:(NSDictionary *)dictionary;
@end
