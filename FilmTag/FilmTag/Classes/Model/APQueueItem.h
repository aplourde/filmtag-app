//
//  APQueueItem.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APVideoMedia.h"

enum {
    APQueueItemStatusSearching,
    APQueueItemStatusNotFound,
    APQueueItemStatusReady,
    APQueueItemStatusProcessing,
    APQueueItemStatusSuccess,
    APQueueItemStatusError
} typedef APQueueItemStatus;

@interface APQueueItem : NSObject {

    NSMutableArray *_medias;
    NSImage *_image;
    NSInteger _currentIndex;
    NSString *_filePath;
    double _progress;

    APQueueItemStatus _status;
    NSString *_statusMessage;
}

@property(readonly) NSMutableArray *medias;
@property(nonatomic, retain) NSString *filePath;
@property(nonatomic) NSInteger currentIndex;
@property(nonatomic) double progress;
@property(nonatomic) APQueueItemStatus status;
@property(nonatomic, retain) NSString *statusMessage;

- (NSImage *)image;

- (APVideoMedia *)currentMedia;

@end
