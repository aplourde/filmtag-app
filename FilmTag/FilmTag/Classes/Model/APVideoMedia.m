//
//  APVideoMedia.m
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APVideoMedia.h"
#import "NSDictionary+APSafeDictionary.h"


@implementation APVideoMedia

@synthesize contentId = _contentId;
@synthesize title = _title;
@synthesize synopsis = _synopsis;
@synthesize imageURL = _imageURL;
@synthesize genre = _genre;
@synthesize contentRating = _contentRating;
@synthesize cast = _cast;
@synthesize directors = _directors;
@synthesize producers = _producers;
@synthesize screenwriters = _screenwriters;
@synthesize studio = _studio;
@synthesize year = _year;
@synthesize isHD = _isHD;
@synthesize isWidescreen = _isWidescreen;



- (NSDictionary *)availableContentRatings
{

    return @{
        @"fr": @{
            @"U":@"fr-movie|Tout Public|100|",
            @"12":@"fr-movie|-12|300|",
            @"10":@"fr-movie|-10|100|",
            @"16":@"fr-movie|-16|375|",
            @"18":@"fr-movie|-18|400|"
        },
        @"au": @{
            @"G":@"au-movie|G|100|",
            @"R18+":@"au-movie|R18+|400|",
            @"PG":@"au-movie|PG|200|",
            @"M":@"au-movie|M|350|",
            @"MA15+":@"au-movie|MA15+|375|",
        },
        @"gb": @{
            @"15":@"uk-movie|15|350|",
            @"R18":@"uk-movie|R18|600|",
            @"U":@"uk-movie|U|100|",
            @"PG":@"uk-movie|PG|200|",
            @"12A":@"uk-movie|12A|325|",
            @"12":@"uk-movie|12|300|",
            @"18":@"uk-movie|18|400|"
        },
        @"ca": @{
            @"18A":@"ca-movie|18A|350|",
            @"G":@"ca-movie|G|100|",
            @"PG":@"ca-movie|PG|200|",
            @"14A":@"ca-movie|14A|300|",
            @"R":@"ca-movie|R|400|",
            @"A":@"ca-movie|A|500|"
        },
        @"de": @{
            @"0":@"de-movie|ab 0 Jahren|75|",
            @"6":@"de-movie|ab 6 Jahren|100|",
            @"12":@"de-movie|ab 12 Jahren|200|",
            @"16":@"de-movie|ab 16 Jahren|500|",
            @"18":@"de-movie|ab 18 Jahren|600|"
        },
        @"us": @{
            @"G":@"mpaa|G|100|",
            @"PG-13":@"mpaa|PG-13|300|",
            @"R":@"mpaa|R|400|",
            @"NC-17":@"mpaa|NC-17|500|",
            @"NR":@"mpaa|NR|000|",
            @"PG":@"mpaa|PG|200|"
        }
    };
}

- (NSString *)iTunesCertificationForCountry:(NSString *)country andCode:(NSString *)code
{
    if ([self.availableContentRatings objectForKeyNotNull:country])
    {
        return [[self.availableContentRatings objectForKey:country] safeStringForKey:code];
    }
    return nil;
}

- (NSString *)searchQuery
{
    return self.title;
}

@end
