//
//  APMainWindowController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APMainWindowController.h"
#import "APMediaInfoViewController.h"
#import "APLoadingViewController.h"
#import "APMediaFinderService.h"
#import "APMovie.h"
#import "APTVShow.h"
#import "APQueueWindowController.h"
#import "APAppDelegate.h"

@implementation APMainWindowController

@synthesize currentFilePath = _currentFilePath;

- (id)initWithWindow:(NSWindow *)window {

    self = [super initWithWindow:window];
    if (self) {
        [[self window] setReleasedWhenClosed:YES];
    }

    return self;
}

- (void)windowDidLoad {

    [super windowDidLoad];
}

- (void)close {
    self.window = nil;
}

- (void)reset {
    [[self window] setContentView:_dropZoneView];
    [[self window] setTitle:@"FilmTag"];
}

- (void)fileDropped:(NSArray *)files {

    if ([files count] < 1) { // no compatible files
        NSAlert *errorAlert = [NSAlert alertWithMessageText:@"Incompatible file type" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"The movie file must be an iTunes compatible file. Either a mp4, m4v or mov type of file."];
        [errorAlert beginSheetModalForWindow:[self window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
        return;
    } else if ([files count] > 1) { //many files, will use the queue mode
        [[[APQueueWindowController sharedWindowController] window] makeKeyAndOrderFront:nil];
        [[APQueueWindowController sharedWindowController] addFiles:files];
        return;
    }

    NSString *path = [files objectAtIndex:0];

    [[self window] setContentView:[[self loadingViewController] view]];
    [[self loadingViewController] setMessage:@"Retrieving info..."];
    [[self loadingViewController] setLoadingStyle:NSProgressIndicatorSpinningStyle];
    [[self loadingViewController] setInfiniteLoading:YES];
    [[self loadingViewController] startLoading];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        NSArray *medias = [[APMediaFinderService sharedInstance] findMediasForFilePath:path];

        dispatch_async(dispatch_get_main_queue(), ^{

            _currentFilePath = path;

            [[self window] setContentView:[[self mediaInfoViewController] view]];
            [[self mediaInfoViewController] setMedias:medias];
            [CATransaction flush];
        });
    });


}

- (IBAction)queue:(id)sender {
    APAppDelegate *appdelegate = (APAppDelegate*)[NSApp delegate];
    [appdelegate queue:nil];
}

- (void)processMedia:(APVideoMedia *)media {

    [[self mediaProcessingService] setDelegate:self];

    [[self window] setContentView:[[self loadingViewController] view]];

    [[self loadingViewController] setLoadingStyle:NSProgressIndicatorSpinningStyle];
    [[self loadingViewController] setInfiniteLoading:NO];
    [[self loadingViewController] setMessage:@"Processing"];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        APVideoMedia *fetchedMedia;
        if ([media isKindOfClass:[APMovie class]]) {
            fetchedMedia = [[APMediaFinderService sharedInstance] fetchMovieMetadata:(APMovie *) media];
        } else {
            fetchedMedia = [[APMediaFinderService sharedInstance] fetchTVShowMetadata:(APTVShow *) media];
        }

        dispatch_async(dispatch_get_main_queue(), ^{

            [[self loadingViewController] setMessage:@"Processing file..."];

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

                [[self mediaProcessingService] processMediaAtPath:_currentFilePath usingMediaInfo:fetchedMedia];

            });
        });
    });
}

- (void)progressChanged:(double)newProgress {

    [[self loadingViewController] setLoadingProgress:newProgress];
}

- (void)success {

    [[self loadingViewController] done];
    [[self loadingViewController] setMessage:@"Completed with success"];
}

- (void)error:(NSString *)errorMessage {

    [[self loadingViewController] error];
    [[self loadingViewController] setMessage:errorMessage];
    NSLog(@"An Error occured while processing media : %@", errorMessage);
}

- (APLoadingViewController *)loadingViewController {

    if (_loadingViewController == nil) {
        _loadingViewController = [[APLoadingViewController alloc] initWithNibName:@"APLoadingViewController" bundle:nil];
    }
    return _loadingViewController;
}

- (APMediaInfoViewController *)mediaInfoViewController {

    if (_mediaInfoViewController == nil) {
        _mediaInfoViewController = [[APMediaInfoViewController alloc] initWithNibName:@"APMediaInfoViewController" bundle:nil];
    }
    return _mediaInfoViewController;
}

- (APMediaProcessingService *)mediaProcessingService {

    if (_mediaProcessingService == nil) {
        _mediaProcessingService = [[APMediaProcessingService alloc] init];
    }
    return _mediaProcessingService;
}

@end
