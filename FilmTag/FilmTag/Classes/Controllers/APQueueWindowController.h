//
//  APQueueWindowController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-27.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "APSearchPopoverController.h"
#import "APMediaProcessingService.h"

@interface APQueueWindowController : NSWindowController <NSTableViewDelegate, NSTableViewDataSource, APSearchPopoverDelegate, APMovieProcessingDelegate> {

    IBOutlet NSTableView *_tableView;
    IBOutlet NSButton *_startButton;
    IBOutlet NSButton *_stopButton;
    IBOutlet NSButton *_clearButton;

    NSMutableArray *_items;
    APSearchPopoverController *_searchPopoverController;
    APMediaProcessingService *_movieProcessingService;
    NSInteger _currentSearchIndex;
    BOOL _stopRequest;
}

+ (id)sharedWindowController;

- (void)addFiles:(NSArray *)files;

- (IBAction)nextMovie:(id)sender;

- (IBAction)previousMovie:(id)sender;

- (IBAction)showSearchPopover:(id)sender;

- (IBAction)removeMovie:(id)sender;

- (IBAction)start:(id)sender;

- (IBAction)stop:(id)sender;

- (IBAction)clean:(id)sender;

- (IBAction)showMainWindow:(id)sender;

@end
