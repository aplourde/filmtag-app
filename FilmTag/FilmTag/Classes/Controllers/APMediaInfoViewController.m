//
//  APMediaInfoViewController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APMediaInfoViewController.h"
#import "APMovie.h"
#import "APMediaProcessingService.h"
#import "APMainWindowController.h"

@implementation APMediaInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }

    return self;
}

#pragma mark - Public Methods -

- (void)setMedias:(NSArray *)medias {

    _medias = medias;
    _currentIndex = 0;

    if ([medias count] > 0) {
        _currentMedia = [medias objectAtIndex:0];

    } else {
        _currentMedia = nil;
    }
    if ([medias count] > 1) {
        [_nextButton setHidden:NO];
        [_previousButton setHidden:NO];
    } else {
        [_nextButton setHidden:YES];
        [_previousButton setHidden:YES];
    }

    [self _refreshCurrentMedia];
}

#pragma mark - Private Methods -

- (void)_refreshCurrentMedia {

    NSImage *image;
    if (_currentMedia != nil) {
        [[self.view window] setTitle:_currentMedia.description];
        image = [[NSImage alloc] initWithContentsOfURL:_currentMedia.imageURL];
        [_processButton setEnabled:YES];
    } else {
        [[self.view window] setTitle:@"No Match Found"];
        image = [NSImage imageNamed:@"not-found"];
        [_processButton setEnabled:NO];
    }
    [_mediaArtwork setImage:image];
    [[self view] setNeedsDisplay:YES];
}

#pragma mark - IB Actions -

- (IBAction)processMedia:(id)sender {

    [(APMainWindowController *) [[[self view] window] windowController] processMedia:_currentMedia];
}

- (IBAction)nextResult:(id)sender {

    if ([_medias count] > 1) {
        _currentIndex = (_currentIndex + 1) % [_medias count];
        _currentMedia = [_medias objectAtIndex:_currentIndex];
        [self _refreshCurrentMedia];
    }
}

- (IBAction)previousResult:(id)sender {

    if ([_medias count] > 1) {
        _currentIndex = (int) _currentIndex - 1 < 0 ? _currentIndex - 1 + (int) [_medias count] : _currentIndex - 1;
        _currentMedia = [_medias objectAtIndex:_currentIndex];
        [self _refreshCurrentMedia];
    }

}

- (IBAction)showSearchPopover:(id)sender {

    if (_searchPopoverController == nil) {
        _searchPopoverController = [[APSearchPopoverController alloc] initWithNibName:@"APSearchPopoverController" bundle:nil];
        _searchPopoverController.delegate = self;
        [_searchPopoverController view]; //initializing the view
    }
    [_searchPopoverController setSearchResults:_medias];
    [_searchPopoverController setQuery:_currentMedia != nil ? _currentMedia.searchQuery : @""];
    [_searchPopoverController.popover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

#pragma mark - APSearchPopover Delegate Methods -

- (void)refreshCurrentMedias:(NSArray *)medias currentIndex:(NSInteger)index {

    [self setMedias:medias];
    _currentIndex = index;
    _currentMedia = [_medias objectAtIndex:index];
    [self _refreshCurrentMedia];
    [_searchPopoverController.popover close];
}

@end
