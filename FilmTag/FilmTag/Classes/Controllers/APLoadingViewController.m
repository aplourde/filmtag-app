//
//  APLoadingViewController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APLoadingViewController.h"
#import "APMainWindowController.h"

@interface APLoadingViewController ()

@end

@implementation APLoadingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }

    return self;
}

- (void)setMessage:(NSString *)message {

    [_messageLabel setStringValue:message];
}

- (void)setInfiniteLoading:(BOOL)isInfinite {

    [_progressIndicator setIndeterminate:isInfinite];
}

- (void)setLoadingStyle:(NSProgressIndicatorStyle)style {

    [_progressIndicator setStyle:style];
}

- (void)setLoadingProgress:(double)progress {

    [_progressIndicator setDoubleValue:progress];
}

- (void)startLoading {

    [_statusImage setHidden:YES];
    [_closeButton setHidden:YES];
    [_progressIndicator setHidden:NO];
    [_progressIndicator startAnimation:nil];
}

- (void)stopLoading {

    [_progressIndicator stopAnimation:nil];
}

- (void)done {

    [self stopLoading];
    [_statusImage setImage:[NSImage imageNamed:@"done"]];
    [_statusImage setHidden:NO];
    [_progressIndicator setHidden:YES];
    [_closeButton setHidden:NO];
}

- (void)error {

    [self stopLoading];
    [_statusImage setImage:[NSImage imageNamed:@"error"]];
    [_statusImage setHidden:NO];
    [_progressIndicator setHidden:YES];
    [_closeButton setHidden:NO];
}

- (IBAction)close:(id)sender {

    [(APMainWindowController*)[[[self view] window] windowController] reset];
}

@end
