//
//  APQueueWindowController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-27.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APQueueWindowController.h"
#import "APMovie.h"
#import "APQueueItemView.h"
#import "APQueueItem.h"
#import "APDragAndDropUtilities.h"
#import "APMediaFinderService.h"
#import "APTVShow.h"
#import "APAppDelegate.h"

@interface APQueueWindowController ()

@end

@implementation APQueueWindowController

int MAX_CONCURRENT_PROCESSES = 3;

+ (id)sharedWindowController {

    static APQueueWindowController *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[APQueueWindowController alloc] initWithWindowNibName:@"APQueueWindowController"];
    });
    return sharedInstance;
}

- (id)initWithWindow:(NSWindow *)window {
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }

    return self;
}

- (void)windowDidLoad {
    [super windowDidLoad];

    _items = [[NSMutableArray alloc] init];
    [_tableView reloadData];
    [_tableView registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
    [_tableView setDraggingSourceOperationMask:NSDragOperationEvery forLocal:NO];
    [self _refreshToolbar];
}

- (void)addFiles:(NSArray *)files {

    NSMutableArray *filteredFiles = [self _filterAlreadyInListFiles:files];
    NSMutableArray *newItems = [self _createNewItems:filteredFiles];
    [self _addItemsToQueue:newItems];
}

#pragma mark - Private Methods -

- (void)_addItemsToQueue:(NSMutableArray *)newItems {

    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange([_items count], [newItems count])];
    [_tableView insertRowsAtIndexes:indexSet withAnimation:0];

    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    [operationQueue setMaxConcurrentOperationCount:MAX_CONCURRENT_PROCESSES];

    for (NSUInteger i = 0; i < [newItems count]; i++) {

        APQueueItem *item = [newItems objectAtIndex:i];
        NSString *filePath = item.filePath;

        [operationQueue addOperationWithBlock:^{

            NSArray *result = [[APMediaFinderService sharedInstance] findMediasForFilePath:filePath];

            dispatch_async(dispatch_get_main_queue(), ^{

                [item.medias addObjectsFromArray:result];
                if (item.medias.count > 0) {
                    item.status = APQueueItemStatusReady;
                    item.statusMessage = @"Ready";
                } else {
                    item.status = APQueueItemStatusNotFound;
                    item.statusMessage = @"Click the search icon to find movies or series";
                }

                [self _refreshToolbar];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[_items indexOfObject:item]];
                [_tableView reloadDataForRowIndexes:indexSet columnIndexes:[NSIndexSet indexSetWithIndex:0]];
            });
        }];
    }
}

- (NSMutableArray *)_createNewItems:(NSMutableArray *)filteredFiles {

    NSMutableArray *newItems = [[NSMutableArray alloc] initWithCapacity:[filteredFiles count]];
    for (NSString *file in filteredFiles) {
        // we need to make that check again as we are adding more files to _items in the loop
        if (![self _isFileAlreadyInList:file]) {
            APQueueItem *item = [[APQueueItem alloc] init];
            item.status = APQueueItemStatusSearching;
            item.statusMessage = @"Searching...";
            item.filePath = file;
            [_items addObject:item];
            [newItems addObject:item];
        }
    }
    return newItems;
}

- (BOOL)_isFileAlreadyInList:(NSString *)file {

    for (APQueueItem *alreadyInListItem in _items) {
        if ([alreadyInListItem.filePath isEqualToString:file]) {
            return YES;
        }
    }
    return NO;
}

- (NSMutableArray *)_filterAlreadyInListFiles:(NSArray *)files {

    NSMutableArray *filteredFiles = [[NSMutableArray alloc] initWithCapacity:[files count]];
    for (NSString *file in files) {
        BOOL fileAlreadyInList = [self _isFileAlreadyInList:file];
        if (!fileAlreadyInList) {
            [filteredFiles addObject:file];
        }
    }
    return filteredFiles;
}

- (APQueueItemView *)_generateQueueItemForRow:(NSInteger)row {

    APQueueItemView *cell = nil;
    if ([[_tableView subviews] count] > row && [[[_tableView subviews] objectAtIndex:row] isKindOfClass:[APQueueItemView class]]) {
        cell = [[_tableView subviews] objectAtIndex:row];
    }
    else {
        cell = [_tableView makeViewWithIdentifier:@"QueueItem" owner:self];
    }

    APQueueItem *currentItem = [_items objectAtIndex:row];

    [cell updateWithQueueItem:currentItem];

    return cell;
}

- (APMediaProcessingService *)_movieProcessingService {

    if (_movieProcessingService == nil) {
        _movieProcessingService = [[APMediaProcessingService alloc] init];
        _movieProcessingService.delegate = self;
    }
    return _movieProcessingService;
}

- (NSInteger)_currentProcessingIndex {

    for (NSInteger i = 0; i < [_items count]; i++) {
        APQueueItem *item = [_items objectAtIndex:i];
        if (item.status == APQueueItemStatusProcessing) {
            return i;
        }
    }
    return -1;
}

- (NSInteger)_lastProcessedIndex {

    for (NSInteger i = [_items count] - 1; i >= 0; i--) {
        APQueueItem *item = [_items objectAtIndex:i];
        if (item.status == APQueueItemStatusSuccess || item.status == APQueueItemStatusError) {
            return i;
        }
    }
    return -1;
}

- (void)_processNextItem {

    [self _refreshToolbar];

    NSInteger lastProcessedIndex = [self _lastProcessedIndex];
    NSInteger nextIndex = lastProcessedIndex == -1 ? 0 : lastProcessedIndex + 1;
    NSInteger toProcess = -1;

    for (NSInteger i = nextIndex; i < [_items count]; i++) {
        APQueueItem *item = [_items objectAtIndex:i];
        if (item.status == APQueueItemStatusReady) {
            toProcess = i;
            break;
        } else if (i == [_items count] - 1) {
            NSAlert *doneAlert = [NSAlert alertWithMessageText:@"The FilmTag queue is done" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"All of the files in the queue have been processed."];
            [doneAlert beginSheetModalForWindow:[self window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
            return;
        }
    }

    if (toProcess != -1) {
        [self _processItemAtIndex:toProcess];
    }
}

- (void)_processItemAtIndex:(NSInteger)index {

    if (_stopRequest) {
        _stopRequest = NO;
        return;
    }

    APQueueItem *item = [_items objectAtIndex:index];
    item.status = APQueueItemStatusProcessing;
    item.statusMessage = @"Fetching data...";

    [self _refreshToolbar];
    [_tableView scrollRowToVisible:index];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        APVideoMedia *fetchedMedia = nil;
        if ([item.currentMedia isKindOfClass:[APMovie class]]) {
            fetchedMedia = [[APMediaFinderService sharedInstance] fetchMovieMetadata:(APMovie *) item.currentMedia];
        } else {
            fetchedMedia = [[APMediaFinderService sharedInstance] fetchTVShowMetadata:(APTVShow *) item.currentMedia];
        }

        dispatch_async(dispatch_get_main_queue(), ^{

            item.statusMessage = @"Processing file...";
            [self _reloadRowAtIndex:index];

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

                [[self _movieProcessingService] processMediaAtPath:item.filePath usingMediaInfo:fetchedMedia];

            });
        });
    });
}

- (void)_refreshToolbar {

    BOOL somethingInProgress = [self _currentProcessingIndex] != -1;
    NSInteger processedFile = 0;
    NSInteger unProcessedFile = 0;
    if (!somethingInProgress) {
        for (APQueueItem *item in _items) {
            if (item.status == APQueueItemStatusSuccess || item.status == APQueueItemStatusError) {
                processedFile++;
            } else if (item.status == APQueueItemStatusReady) {
                unProcessedFile++;
            }
        }
    }

    [_startButton setEnabled:unProcessedFile > 0 && !somethingInProgress];
    [_stopButton setEnabled:somethingInProgress && !_stopRequest];
    [_clearButton setEnabled:processedFile > 0 && !somethingInProgress];
}

- (void)_reloadRowAtIndex:(NSInteger)index {

    [_tableView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:index] columnIndexes:[NSIndexSet indexSetWithIndex:0]];
}

#pragma mark - IB Actions -

- (IBAction)nextMovie:(id)sender {

    NSInteger index = [_tableView rowForView:sender];
    APQueueItem *currentItem = [_items objectAtIndex:index];
    if ([currentItem.medias count] > 1) {
        currentItem.currentIndex = (currentItem.currentIndex + 1) % [currentItem.medias count];
        [self _reloadRowAtIndex:index];
    }
}

- (IBAction)previousMovie:(id)sender {

    NSInteger index = [_tableView rowForView:sender];
    APQueueItem *currentItem = [_items objectAtIndex:index];
    if ([currentItem.medias count] > 1) {
        currentItem.currentIndex = (int) currentItem.currentIndex - 1 < 0 ? currentItem.currentIndex - 1 + (int) [currentItem.medias count] : currentItem.currentIndex - 1;
        [self _reloadRowAtIndex:index];
    }
}

- (IBAction)showSearchPopover:(id)sender {

    _currentSearchIndex = [_tableView rowForView:sender];
    APQueueItem *currentItem = [_items objectAtIndex:_currentSearchIndex];

    if (_searchPopoverController == nil) {
        _searchPopoverController = [[APSearchPopoverController alloc] initWithNibName:@"APSearchPopoverController" bundle:nil];
        _searchPopoverController.delegate = self;
        [_searchPopoverController view]; //initializing the view
    }
    [_searchPopoverController setQuery:currentItem.currentMedia != nil ? currentItem.currentMedia.searchQuery : @""];
    [_searchPopoverController setSearchResults:[currentItem medias]];
    [_searchPopoverController.popover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxXEdge];
}

- (IBAction)removeMovie:(id)sender {

    NSInteger index = [_tableView rowForView:sender];

    [_items removeObjectAtIndex:index];
    [_tableView removeRowsAtIndexes:[NSIndexSet indexSetWithIndex:index] withAnimation:NSTableViewAnimationEffectFade];
    [self _refreshToolbar];
}

- (IBAction)start:(id)sender {

    [self clean:nil];
    [self _processNextItem];
}

- (IBAction)stop:(id)sender {

    _stopRequest = YES;
}

- (IBAction)clean:(id)sender {

    NSMutableIndexSet *toRemove = [NSMutableIndexSet indexSet];
    for (NSInteger i = 0; i < [_items count]; i++) {
        APQueueItem *item = [_items objectAtIndex:i];
        if (item.progress == 100.0) {
            [toRemove addIndex:i];
        }
    }
    [_tableView removeRowsAtIndexes:toRemove withAnimation:NSTableViewAnimationEffectFade];
    [_items removeObjectsAtIndexes:toRemove];
    [self _refreshToolbar];
}

- (IBAction)showMainWindow:(id)sender {
    
    APAppDelegate *appDelegate = [NSApp delegate];
    if (![appDelegate freeWindowAvailable]) {
        [appDelegate newWindow:nil];
    }
    
    [[appDelegate freeWindow] makeKeyAndOrderFront:nil];
}


#pragma mark - NSDataTable Delegate -

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {

    return [self _generateQueueItemForRow:row];
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {

    return [_items count];
}

- (BOOL)tableView:(NSTableView *)aTableView acceptDrop:(id <NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation {

    NSArray *droppedFiles = [[APDragAndDropUtilities sharedInstance] extractMP4FilesFromDraggingInfo:info];

    if (droppedFiles != nil && [droppedFiles count] > 0) {
        [self addFiles:droppedFiles];
        return YES;
    } else {
        NSAlert *errorAlert = [NSAlert alertWithMessageText:@"Incompatible file type" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"No compatible file types were found. Files must be either mp4, m4v or mov."];
        [errorAlert beginSheetModalForWindow:[self window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
    }
    return NO;
}

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id <NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation {

    [aTableView setDropRow:[_items count] dropOperation:NSTableViewDropAbove];

    NSArray *filesToDrop = [[APDragAndDropUtilities sharedInstance] extractMP4FilesFromDraggingInfo:info];
    if ([filesToDrop count] > 0) {
        return NSDragOperationCopy;
    }

    return NSDragOperationNone;
}

#pragma mark - APSearchPopover Delegate Methods -

- (void)refreshCurrentMedias:(NSArray *)medias currentIndex:(NSInteger)index {

    APQueueItem *currentItem = [_items objectAtIndex:_currentSearchIndex];
    [currentItem.medias setArray:medias];
    currentItem.currentIndex = index;
    currentItem.status = APQueueItemStatusReady;
    currentItem.statusMessage = @"Ready";

    [self _refreshToolbar];
    [self _reloadRowAtIndex:_currentSearchIndex];
}

#pragma mark - APMovieProcessingService Delegate Methods -

- (void)progressChanged:(double)newProgress {

    NSInteger processingIndex = [self _currentProcessingIndex];
    APQueueItem *item = [_items objectAtIndex:processingIndex];
    item.status = APQueueItemStatusProcessing;
    item.statusMessage = [NSString stringWithFormat:@"Processing... (%ld%%)", (long) newProgress];
    item.progress = newProgress;

    [self _reloadRowAtIndex:processingIndex];
}

- (void)success {

    NSInteger processingIndex = [self _currentProcessingIndex];
    APQueueItem *item = [_items objectAtIndex:processingIndex];
    item.status = APQueueItemStatusSuccess;
    item.progress = 100;
    item.statusMessage = @"Done";
    [self _reloadRowAtIndex:processingIndex];
    [self _processNextItem];
}

- (void)error:(NSString *)errorMessage {

    NSInteger processingIndex = [self _currentProcessingIndex];
    APQueueItem *item = [_items objectAtIndex:processingIndex];
    item.status = APQueueItemStatusError;
    item.statusMessage = [NSString stringWithFormat:@"Error: %@,", errorMessage];
    item.progress = 100;
    [self _reloadRowAtIndex:processingIndex];
    [self _processNextItem];
}

@end
