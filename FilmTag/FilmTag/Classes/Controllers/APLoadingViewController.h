//
//  APLoadingViewController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface APLoadingViewController : NSViewController {

    IBOutlet NSTextField *_messageLabel;
    IBOutlet NSProgressIndicator *_progressIndicator;
    IBOutlet NSImageView *_statusImage;
    IBOutlet NSButton *_closeButton;
}

- (void)setMessage:(NSString *)message;

- (void)setInfiniteLoading:(BOOL)isInfinite;

- (void)setLoadingStyle:(NSProgressIndicatorStyle)style;

- (void)setLoadingProgress:(double)progress;

- (void)startLoading;

- (void)stopLoading;

- (void)done;

- (void)error;

- (IBAction)close:(id)sender;

@end
