//
//  APSearchPopoverViewController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class APMovie;

@protocol APSearchPopoverDelegate <NSObject>

- (void)refreshCurrentMedias:(NSArray *)medias currentIndex:(NSInteger)index;

@end

@interface APSearchPopoverController : NSViewController <NSTableViewDataSource, NSTableViewDelegate> {

    IBOutlet NSSearchField *_searchField;
    IBOutlet NSProgressIndicator *_searchSpinner;
    IBOutlet NSTableView *_tableView;
    IBOutlet NSPopover *_searchPopover;

    NSArray *_searchResults;
    NSInteger _currentIndex;

    id <APSearchPopoverDelegate> _delegate;
}

- (IBAction)search:(id)sender;

- (IBAction)searchSelectionChange:(id)sender;

- (void)setQuery:(NSString *)query;

- (void)setSearchResults:(NSArray *)results;

@property(nonatomic, retain) id <APSearchPopoverDelegate> delegate;
@property(nonatomic, retain) NSPopover *popover;

@end
