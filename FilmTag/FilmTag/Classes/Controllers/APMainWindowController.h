//
//  APMainWindowController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import "APDropZone.h"
#import "APMediaProcessingService.h"

@class APLoadingViewController;
@class APMediaInfoViewController;
@class APMovie;

@interface APMainWindowController : NSWindowController <APDropZoneDelegate, APMovieProcessingDelegate> {

    IBOutlet APDropZone *_dropZoneView;
    APLoadingViewController *_loadingViewController;
    APMediaInfoViewController *_mediaInfoViewController;
    APMediaProcessingService *_mediaProcessingService;

    NSString *_currentFilePath;
}

- (void)fileDropped:(NSArray *)files;

- (IBAction)processMedia:(APVideoMedia *)media;

- (void)reset;

@property(nonatomic, retain) NSString *currentFilePath;

@end
