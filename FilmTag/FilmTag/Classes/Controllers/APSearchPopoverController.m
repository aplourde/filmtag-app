//
//  APSearchPopoverViewController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APSearchPopoverController.h"
#import "APMediaFinderService.h"
#import "APMovie.h"
#import "APTVShow.h"
#import "APSearchResultItemView.h"

@implementation APSearchPopoverController

@synthesize delegate = _delegate;
@synthesize popover = _searchPopover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }

    return self;
}

#pragma mark - Public Methods -

- (void)setQuery:(NSString *)query {

    [_searchField setStringValue:query];
}

- (void)setSearchResults:(NSArray *)searchResults {

    _searchResults = searchResults;
    [_tableView reloadData];
}


#pragma mark - IB Actions -

- (IBAction)search:(id)sender {

    [_searchSpinner setHidden:NO];
    [_searchSpinner startAnimation:nil];
    _searchResults = nil;
    _searchResults = [NSArray array];
    [_tableView reloadData];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        _searchResults = [[APMediaFinderService sharedInstance] findMediasForKeywords:[sender stringValue]];

        dispatch_async(dispatch_get_main_queue(), ^{

            [_tableView reloadData];
            [_searchSpinner stopAnimation:nil];
            [_searchSpinner setHidden:YES];

        });
    });
}

- (IBAction)searchSelectionChange:(id)sender {

    NSInteger selectedRow = [_tableView selectedRow];

    if (selectedRow != -1) {
        _currentIndex = selectedRow;
        if (_delegate) {
            [_delegate refreshCurrentMedias:_searchResults currentIndex:_currentIndex];
        }
    }
}

#pragma mark - NSTableView Delegate and Datasource Methods -

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {

    return _searchResults != nil ? [_searchResults count] : 0;
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {

    APVideoMedia *currentMedia = [_searchResults objectAtIndex:row];

    APSearchResultItemView *cell = nil;
    if ([[tableView subviews] count] > row && [[[tableView subviews] objectAtIndex:row] isKindOfClass:[APSearchResultItemView class]]) {
        cell = [[tableView subviews] objectAtIndex:row];
    }
    else {
        cell = [tableView makeViewWithIdentifier:@"MovieCell" owner:self];
    }

    if ([currentMedia isKindOfClass:[APMovie class]]) {
        cell.textField.stringValue = currentMedia.title;
        cell.info.stringValue = currentMedia.year > 0 ? [NSString stringWithFormat:@"(%ld)", currentMedia.year] : @"";
        cell.subInfo.stringValue = @"";

    } else {
        APTVShow *tvShow = (APTVShow*) currentMedia;
        cell.textField.stringValue = tvShow.seriesTitle;
        cell.info.stringValue = [NSString stringWithFormat:@"Season %ld Episode %ld", tvShow.seasonNumber, tvShow.episodeNumber];
        cell.subInfo.stringValue = tvShow.title;

    }
    cell.imageView.image = [[NSImage alloc] initWithContentsOfURL:currentMedia.imageURL];

    return cell;
}

@end
