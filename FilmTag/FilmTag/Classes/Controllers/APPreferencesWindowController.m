//
//  APPreferencesWindowController.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-27.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APPreferencesWindowController.h"
#import "Constants.h"

@interface APPreferencesWindowController ()

@end

@implementation APPreferencesWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];

    [self setupAvailableLanguages];
    [self setupAvailableCountries];
}


- (void)setupAvailableCountries
{
    _availableCountries = [[NSMutableDictionary alloc] init];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSString *currentCountry = [[NSUserDefaults standardUserDefaults] objectForKey:kAPPreferencesPreferredCountry];
    NSString *currentLocalizedCountry = nil;
    NSArray *countryArray = @[@"us",@"fr",@"au",@"de",@"gb",@"ca"];
    for (NSString *country in countryArray) {
        NSString *lowercaseCountry = [country lowercaseString];
        NSString *localizedCountry = [locale displayNameForKey:NSLocaleCountryCode value:lowercaseCountry];
        [_availableCountries setObject:lowercaseCountry forKey:localizedCountry];
        if ([lowercaseCountry isEqualToString:currentCountry])
        {
            currentLocalizedCountry = localizedCountry;
        }
    }
    NSArray *sortedLocalizedCountries = [[_availableCountries allKeys] sortedArrayUsingSelector:@selector(localizedCompare:)];
    [_countriesSelectBox addItemsWithTitles:sortedLocalizedCountries];
    [_countriesSelectBox selectItemWithTitle:currentLocalizedCountry];
}


- (void)setupAvailableLanguages
{
    _availableLanguages = [[NSMutableDictionary alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *currentLanguage = [defaults objectForKey:kAPPreferencesPreferredLanguage];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    for (NSString *language in [defaults objectForKey:@"AppleLanguages"]) {
        NSString *localizedLanguage = [[locale displayNameForKey:NSLocaleIdentifier value:language] capitalizedString];
        [_languagesSelectBox addItemWithTitle:localizedLanguage];
        [_availableLanguages setObject:language forKey:localizedLanguage];
        if ([currentLanguage isEqualToString:language]) {
            [_languagesSelectBox selectItemWithTitle:localizedLanguage];
        }
    }
}


- (IBAction)countrySelectionChanged:(id)sender {

    NSString *selectedTitle = [[_countriesSelectBox selectedItem] title];
    NSString *selectedCountry = [_availableCountries objectForKey:selectedTitle];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:selectedCountry forKey:kAPPreferencesPreferredCountry];
    [defaults synchronize];
}


- (IBAction)languageSelectionChanged:(id)sender {

    NSString *selectedTitle = [[_languagesSelectBox selectedItem] title];
    NSString *selectedLanguage = [_availableLanguages objectForKey:selectedTitle];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:selectedLanguage forKey:kAPPreferencesPreferredLanguage];
    [defaults synchronize];
}

@end
