//
//  APMediaInfoViewController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-26.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "APSearchPopoverController.h"

@class APMovie;

@interface APMediaInfoViewController : NSViewController <APSearchPopoverDelegate> {

    IBOutlet NSImageView *_mediaArtwork;
    IBOutlet NSButton *_nextButton;
    IBOutlet NSButton *_previousButton;
    IBOutlet NSButton *_processButton;

    APSearchPopoverController *_searchPopoverController;

    NSArray *_medias;
    APMovie *_currentMedia;
    NSInteger _currentIndex;
}

- (void)setMedias:(NSArray *)medias;

- (IBAction)processMedia:(id)sender;

- (IBAction)nextResult:(id)sender;

- (IBAction)previousResult:(id)sender;

- (IBAction)showSearchPopover:(id)sender;

@end
