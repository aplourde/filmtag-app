//
//  APPreferencesWindowController.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-27.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface APPreferencesWindowController : NSWindowController {
    
    IBOutlet NSPopUpButton *_languagesSelectBox;
    IBOutlet NSPopUpButton *_countriesSelectBox;
    NSMutableDictionary *_availableLanguages;
    NSMutableDictionary *_availableCountries;
}

- (IBAction)languageSelectionChanged:(id)sender;
- (IBAction)countrySelectionChanged:(id)sender;

@end
