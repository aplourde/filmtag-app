//
//  APSearchResultItemView.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-11-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APSearchResultItemView.h"

@implementation APSearchResultItemView

@synthesize info = _info;
@synthesize subInfo = _subInfo;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    // Drawing code here.
}

@end
