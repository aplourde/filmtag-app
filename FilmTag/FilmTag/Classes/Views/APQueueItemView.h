//
//  APQueueItemView.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-28.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class APQueueItem;

@interface APQueueItemView : NSTableCellView {

    IBOutlet NSTextField *_title;
    IBOutlet NSTextField *_year;
    IBOutlet NSTextField *_status;
    IBOutlet NSImageView *_statusImage;
    IBOutlet NSTextField *_fileName;
    IBOutlet NSImageView *_artwork;
    IBOutlet NSProgressIndicator *_progressBar;

    IBOutlet NSButton *_leftArrow;
    IBOutlet NSButton *_rightArrow;
    IBOutlet NSButton *_searchButton;
    IBOutlet NSButton *_deleteButton;
    IBOutlet NSProgressIndicator *_loading;
}

- (void)updateWithQueueItem:(APQueueItem *)item;

@property(nonatomic, retain) NSString *title;
@property(nonatomic) NSInteger year;
@property(nonatomic, retain) NSString *info;
@property(nonatomic, retain) NSString *status;
@property(nonatomic, retain) NSString *fileName;
@property(nonatomic, retain) NSImage *artwork;
@property(nonatomic) double progress;
@property(nonatomic) BOOL isLoading;

@property(nonatomic, retain) NSButton *leftArrow;
@property(nonatomic, retain) NSButton *rightArrow;
@property(nonatomic, retain) NSButton *searchButton;
@property(nonatomic, retain) NSButton *deleteButton;

@end
