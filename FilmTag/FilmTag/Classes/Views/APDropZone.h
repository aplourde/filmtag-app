//
//  APDropZone.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-24.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol APDropZoneDelegate <NSObject>

- (void)fileDropped:(NSArray *)files;

@end

@interface APDropZone : NSView <NSDraggingDestination> {
    BOOL _mouseover;
    NSImage *_baseImage;
    NSImage *_overImage;

    IBOutlet id <APDropZoneDelegate> delegate;
}

@end
