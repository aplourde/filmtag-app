//
//  APQueueItemView.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-07-28.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APQueueItemView.h"
#import "APQueueItem.h"
#import "APMovie.h"
#import "APTVShow.h"

@implementation APQueueItemView

@synthesize leftArrow = _leftArrow;
@synthesize rightArrow = _rightArrow;
@synthesize searchButton = _searchButton;
@synthesize deleteButton = _deleteButton;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }

    return self;
}

- (void)drawRect:(NSRect)dirtyRect {
    // Drawing code here.
}

- (NSString *)status {

    return [_status stringValue];
}

- (void)updateWithQueueItem:(APQueueItem *)item {

    if (item.status == APQueueItemStatusNotFound) {
        self.title = @"Not Match Found";
        self.artwork = [NSImage imageNamed:@"alt-art-not-found"];
    } else {
        self.title = item.currentMedia != nil ? item.currentMedia.title : @"";
        self.artwork = item.image;
    }
    self.fileName = item.filePath;
    if (item.status == APQueueItemStatusSearching || item.status == APQueueItemStatusNotFound) {
        self.year = 0;
        self.info = @"";
        self.title = @"";
    } else {
        if ([item.currentMedia isKindOfClass:[APMovie class]]) {
            self.year = item.currentMedia != nil ? item.currentMedia.year : 0;
        } else {
            APTVShow *tvShow = (APTVShow*) item.currentMedia;
            self.title = tvShow != nil ? tvShow.seriesTitle : @"";
            self.info = [NSString stringWithFormat:@"Season %ld Episode %ld", tvShow.seasonNumber, tvShow.episodeNumber];
        }
    }
    self.status = item.statusMessage;
    self.progress = item.status == APQueueItemStatusProcessing && item.progress == 0 ? 1 : item.progress;
    self.isLoading = item.status == APQueueItemStatusSearching;

    //enabling/disabling buttons
    switch (item.status) {
        case APQueueItemStatusSearching:
        case APQueueItemStatusProcessing:
            [self.leftArrow setHidden:YES];
            [self.rightArrow setHidden:YES];
            [self.searchButton setHidden:YES];
            [self.deleteButton setHidden:YES];
            break;
        case APQueueItemStatusReady:
        case APQueueItemStatusNotFound:
            [self.leftArrow setHidden:item.medias.count < 2];
            [self.rightArrow setHidden:item.medias.count < 2];
            [self.searchButton setHidden:NO];
            [self.deleteButton setHidden:NO];
            break;
        case APQueueItemStatusSuccess:
        case APQueueItemStatusError:
            [self.leftArrow setHidden:YES];
            [self.rightArrow setHidden:YES];
            [self.searchButton setHidden:YES];
            [self.deleteButton setHidden:NO];
            break;
        default:
            break;
    }

    //status image
    switch (item.status) {
        case APQueueItemStatusSearching:
        case APQueueItemStatusProcessing:
            [_statusImage setImage:[NSImage imageNamed:@"queue-item-process"]];
            break;
        case APQueueItemStatusReady:
            [_statusImage setImage:[NSImage imageNamed:@"queue-item-ready"]];
            break;
        case APQueueItemStatusNotFound:
            [_statusImage setImage:[NSImage imageNamed:@"queue-item-not-found"]];
            break;
        case APQueueItemStatusSuccess:
            [_statusImage setImage:[NSImage imageNamed:@"queue-item-done"]];
            break;
        case APQueueItemStatusError:
            [_statusImage setImage:[NSImage imageNamed:@"queue-item-error"]];
            break;
        default:
            break;
    }
}

- (void)setStatus:(NSString *)status {

    [_status setStringValue:status != nil ? status : @""];
}

- (NSString *)fileName {

    return [_fileName stringValue];
}

- (void)setFileName:(NSString *)fileName {

    [_fileName setStringValue:fileName != nil ? [fileName lastPathComponent] : @""];
}

- (NSString *)title {

    return [_title stringValue];
}

- (void)setTitle:(NSString *)title {

    [_title setStringValue:title != nil ? title : @""];
}

- (NSInteger)year {

    if ([[_year stringValue] length] == 6) {
        return [[[_year stringValue] substringWithRange:NSMakeRange(1, 4)] integerValue];
    }
    return 0;
}

- (void)setYear:(NSInteger)year {

    if (year > 1000) {
        [_year setStringValue:[NSString stringWithFormat:@"(%ld)", year]];
    } else {
        [_year setStringValue:@""];
    }
}

- (NSString *)info {
    
    return [_year stringValue];
}

- (void)setInfo:(NSString *)info {
    
    [_year setStringValue:info != nil ? info : @""];
}

- (NSImage *)artwork {

    return [_artwork image];
}

- (void)setArtwork:(NSImage *)image {

    [_artwork setImage:image];
}

- (double)progress {

    return [_progressBar doubleValue];
}

- (void)setProgress:(double)progress {

    if (progress > 0) {
        if ([_progressBar isHidden]) {
            [_progressBar setHidden:NO];
        }
        [_progressBar setDoubleValue:progress];
    } else {
        [_progressBar setHidden:YES];
    }
}

- (BOOL)isLoading {

    return ![_loading isHidden];
}

- (void)setIsLoading:(BOOL)isLoading {

    [_loading setHidden:!isLoading];
    if (isLoading) {
        [_loading startAnimation:nil];
    } else {
        [_loading stopAnimation:nil];
    }
}

@end
