//
//  APDropZone.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-24.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APDropZone.h"
#import "APDragAndDropUtilities.h"

@implementation APDropZone

- (id)initWithFrame:(NSRect)frame {

    self = [super initWithFrame:frame];
    if (self) {
        [self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
        _mouseover = NO;
        _baseImage = [NSImage imageNamed:@"drop-zone.png"];
        _overImage = [NSImage imageNamed:@"drop-zone-over.png"];
    }

    return self;
}

#pragma mark - Destination Operations

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {

    _mouseover = YES;
    if ((NSDragOperationGeneric & [sender draggingSourceOperationMask]) == NSDragOperationGeneric) {
        [self setNeedsDisplay:YES];
        return NSDragOperationGeneric;
    } else {
        return NSDragOperationNone;
    }
}

- (void)draggingExited:(id <NSDraggingInfo>)sender {

    _mouseover = NO;
    [self setNeedsDisplay:YES];
}

- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender {

    if ((NSDragOperationGeneric & [sender draggingSourceOperationMask]) == NSDragOperationGeneric) {
        return NSDragOperationGeneric;
    } else {
        return NSDragOperationNone;
    }
}

- (void)draggingEnded:(id <NSDraggingInfo>)sender {

    _mouseover = NO;
    [self setNeedsDisplay:YES];
}


- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {

    return YES;
}


- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
    
    NSArray *droppedFiles = [[APDragAndDropUtilities sharedInstance] extractMP4FilesFromDraggingInfo:sender];
    BOOL accepted = NO;
    if (droppedFiles != nil) {
        if (delegate) {
            [delegate fileDropped:droppedFiles];
        }
        accepted = YES;
    }
    [self setNeedsDisplay:YES];
    return accepted;
}


- (void)concludeDragOperation:(id <NSDraggingInfo>)sender {

    [self setNeedsDisplay:YES];

}


- (void)drawRect:(NSRect)dirtyRect {

    NSImage *image;
    if (_mouseover) {
        image = _overImage;
    } else {
        image = _baseImage;
    }
    [image drawInRect:[self bounds] fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
}

@end
