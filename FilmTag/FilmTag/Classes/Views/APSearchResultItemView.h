//
//  APSearchResultItemView.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-11-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface APSearchResultItemView : NSTableCellView {
    
    IBOutlet NSTextField *_info;
    IBOutlet NSTextField *_subInfo;
}

@property(nonatomic, retain) NSTextField *info;
@property(nonatomic, retain) NSTextField *subInfo;

@end
