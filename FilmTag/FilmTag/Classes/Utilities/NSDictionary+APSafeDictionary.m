//
//  NSDictionary+APSafeDictionary.m
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "NSDictionary+APSafeDictionary.h"

@implementation NSDictionary (APSafeDictionary)

-(NSString*) safeStringForKey:(NSString*)key {
    if ([self objectForKeyNotNull:key] && [[self objectForKey:key] length] > 0) {
        return [self objectForKey:key];
    }
    return nil;
}

-(NSArray*) safeArrayForKey:(NSString*)key {
    if ([self objectForKeyNotNull:key] && [[self objectForKey:key] isKindOfClass:[NSArray class]]) {
        return [self objectForKey:key];
    }
    return [NSArray array];
}

-(NSString*) safeUrlForKey:(NSString*)key {
    if ([self objectForKeyNotNull:key] && [[self objectForKey:key] count] > 0) {
        @try {
            return [NSURL URLWithString:[self objectForKey:key]];
        }
        @catch (NSException * e) {
            return nil;
        }
    }
    return nil;
}

-(int) safeIntegerForKey:(NSString*)key {

    if ([self objectForKeyNotNull:key]) {
        return [[self objectForKey:key] intValue];
    }
    return -1;
}

-(BOOL) safeBoolForKey:(NSString*)key {
    if ([self objectForKeyNotNull:key]) {
        return [[self objectForKey:key] boolValue];
    }
    return NO;
}

-(BOOL) objectForKeyNotNull:(NSString*) key {
    return [self objectForKey:key] != nil && [self objectForKey:key] != [NSNull null];
}

@end
