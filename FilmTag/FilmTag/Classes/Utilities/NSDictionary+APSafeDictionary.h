//
//  NSDictionary+APSafeDictionary.h
//  FilmTag
//
//  Created by Anthony Plourde on 10/27/2013.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (APSafeDictionary)

-(NSString*) safeStringForKey:(NSString*)key;
-(NSArray*) safeArrayForKey:(NSString*)key;
-(NSString*) safeUrlForKey:(NSString*)key;
-(int) safeIntegerForKey:(NSString*)key;
-(BOOL) safeBoolForKey:(NSString*)key;
-(BOOL) objectForKeyNotNull:(NSString*) key;

@end
