//
//  APDragAndDropUtilities.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APDragAndDropUtilities : NSObject

+ (id)sharedInstance;

- (NSArray *)extractMP4FilesFromDraggingInfo:(id <NSDraggingInfo>)info;

@end
