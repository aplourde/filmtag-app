//
//  APDragAndDropUtilities.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-08-05.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APDragAndDropUtilities.h"

@implementation APDragAndDropUtilities

+ (id)sharedInstance {

    static APDragAndDropUtilities *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSArray *)extractMP4FilesFromDraggingInfo:(id <NSDraggingInfo>)info {

    NSPasteboard *paste = [info draggingPasteboard];
    //gets the dragging-specific pasteboard from the sender
    NSArray *types = [NSArray arrayWithObjects:NSFilenamesPboardType, nil];
    //a list of types that we can accept
    NSString *desiredType = [paste availableTypeFromArray:types];
    NSData *carriedData = [paste dataForType:desiredType];

    if (nil == carriedData) {
        NSRunAlertPanel(@"Paste Error", @"Sorry, but the paste operation failed", nil, nil, nil);
        return nil;
    } else {
        if ([desiredType isEqualToString:NSFilenamesPboardType]) {
            NSArray *fileArray = [[NSMutableArray alloc] initWithArray:[paste propertyListForType:@"NSFilenamesPboardType"]];

            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSMutableArray *compatibleFileArray = [[NSMutableArray alloc] initWithCapacity:[fileArray count]];
            for (NSString *path in fileArray) {

                BOOL isDir;
                if ([fileManager fileExistsAtPath:path isDirectory:&isDir] && isDir) {

                    NSDirectoryEnumerator *directoryEnum = [fileManager enumeratorAtPath:path];
                    NSString *currentFileWithinDirectory;
                    while ((currentFileWithinDirectory = [directoryEnum nextObject])) {

                        if ([self _isFileCompatible:currentFileWithinDirectory]) {
                            [compatibleFileArray addObject:[path stringByAppendingPathComponent:currentFileWithinDirectory]];
                        }
                    }

                } else if ([self _isFileCompatible:path]) {
                    [compatibleFileArray addObject:path];
                }
            }
            return compatibleFileArray;
        }
        else {
            NSAssert(NO, @"This can't happen");
            return nil;
        }
    }
}

- (BOOL)_isFileCompatible:(NSString *)file {

    return [[NSArray arrayWithObjects:@"mov", @"mp4", @"m4v", nil] indexOfObject:[[file pathExtension] lowercaseString]] != NSNotFound;
}

@end
