//
//  APMediaProcessingService.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-25.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APMediaProcessingService.h"
#import "APMovie.h"
#import "APTVShow.h"

@implementation APMediaProcessingService

@synthesize delegate = _delegate;

- (id)init {

    if (self = [super init]) {
    }
    return self;
}

- (void)processMediaAtPath:(NSString *)inputPath usingMediaInfo:(APVideoMedia *)videoMedia {

    if (inputPath == nil || [inputPath length] == 0 || videoMedia == nil) {
        return;
    }

    NSError *error;
    MP4File *file = [[MP4File alloc] initWithFilePath:inputPath outError:&error];
    if (error != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate error:[error localizedDescription]];
        });
        return;
    }
    file.delegate = self;

#ifdef DEBUG
    NSLog(@"\n\nOriginal Media:\n\n");
    [[file metadata] printCurrentTags];
#endif

#ifdef DEBUG
    NSLog(@"\n\nModifying Media...\n\n");
#endif
    videoMedia.isHD = [file videoSize].height > 700;
    videoMedia.isWidescreen = [file videoSize].width / [file videoSize].height >= 1.5;

    [self _generateMetadataForMedia:videoMedia onFile:file];

    [file save:&error];
    if (error != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate error:[error localizedDescription]];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_delegate success];
        });
    }

#ifdef DEBUG
    NSLog(@"\n\nUpdated Media:\n\n");
    [[file metadata] printCurrentTags];
#endif

}

- (void)progressChanged:(double)newProgress {

    dispatch_async(dispatch_get_main_queue(), ^{
        [_delegate progressChanged:newProgress];
    });
}

- (void)_generateMetadataForMedia:(APVideoMedia *)videoMedia onFile:(MP4File *)file {

    file.metadata.name = videoMedia.title;
    file.metadata.genre = videoMedia.genre;

    if (videoMedia.synopsis.length < 255) {
        file.metadata.shortDescription = videoMedia.synopsis;
        file.metadata.comments = videoMedia.synopsis;
    } else {
        file.metadata.shortDescription = [[videoMedia.synopsis substringToIndex:252] stringByAppendingString:@"..."];
        file.metadata.comments = [[videoMedia.synopsis substringToIndex:252] stringByAppendingString:@"..."];
    }

    NSImage *image = [[NSImage alloc] initWithContentsOfURL:videoMedia.imageURL];
    
    if (videoMedia.contentId > 0) {
        file.metadata.contentId = videoMedia.contentId;
    }
    file.metadata.longDescription = videoMedia.synopsis;
    file.metadata.artwork = image;
    file.metadata.type = [videoMedia isKindOfClass:[APMovie class]] ? MP4MediaTypeMovie : MP4MediaTypeTvShow;
    file.metadata.releaseDate = [NSString stringWithFormat:@"%ld", videoMedia.year];
    file.metadata.hd = videoMedia.isHD;
    file.metadata.screenFormat = videoMedia.isWidescreen ? @"widescreen" : nil;
    file.metadata.cast = videoMedia.cast;
    file.metadata.directors = videoMedia.directors;
    file.metadata.screenwriters = videoMedia.screenwriters;
    file.metadata.producers = videoMedia.producers;
    file.metadata.studio = videoMedia.studio;
    file.metadata.contentRating = videoMedia.contentRating;
    if ([videoMedia isKindOfClass:[APTVShow class]]) {
        file.metadata.show = [(APTVShow *) videoMedia seriesTitle];
        file.metadata.season = [(APTVShow *) videoMedia seasonNumber];
        file.metadata.episode = [(APTVShow *) videoMedia episodeNumber];
    }
}

@end
