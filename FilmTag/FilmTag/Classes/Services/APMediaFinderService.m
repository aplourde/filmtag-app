//
//  APMediaFinderService.m
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-25.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import "APMediaFinderService.h"
#import "APMovie.h"
#import "APTVShow.h"
#import "Constants.h"
#import "NSDictionary+APSafeDictionary.h"

@implementation APMediaFinderService

+ (id)sharedInstance {

    static APMediaFinderService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        _acceptHeader = @"application/json";
        _contentTypeHeader = @"application/json";
    }
    return self;
}

- (NSArray *)findMediasForFilePath:(NSString *)filePath {

    NSString *fileName = [self _fileNameFromPath:filePath];
    NSString *folder = [self _fileFolderFromPath:filePath];
    NSString *parentFolder = [self _fileParentFolderFromPath:filePath];
    NSString *version = [self currentAppVersion];

    NSString *stringUrl = [[kAdmitOneApiEndPoint stringByAppendingFormat:kAdmitOneApiResourceResolve, fileName, folder, parentFolder, version] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [self _findMediasUsingURL:stringUrl];
}

- (NSString *)currentAppVersion {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

- (NSArray *)findMediasForKeywords:(NSString *)keywords {
    NSString *version = [self currentAppVersion];
    NSString *stringUrl = [[kAdmitOneApiEndPoint stringByAppendingFormat:kAdmitOneApiResourceSearch, [keywords stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]], version] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [self _findMediasUsingURL:stringUrl];
}

- (APMovie *)fetchMovieMetadata:(APMovie *)movieToFetch {

    //fetching with default language (en)
    NSURL *movieURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceMovie, [movieToFetch.imdbId integerValue], @"en"]];
    NSDictionary *dict = (id) [self performActionRequestToURL:movieURL usingCache:YES withMethod:@"GET" body:nil];
    APMovie *movie = nil;
    if (dict != nil) {
        movie = [[APMovie alloc] initWithTheMovieDBDictionary:dict];
    }

    //fetching with users preferred language if different
    NSString *preferredLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:kAPPreferencesPreferredLanguage];
    if (![preferredLanguage isEqualToString:@"en"]) {
        movieURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceMovie, [movieToFetch.imdbId integerValue], preferredLanguage]];
        dict = (id) [self performActionRequestToURL:movieURL usingCache:YES withMethod:@"GET" body:nil];
        APMovie *localizedMovie = nil;
        if (dict != nil) {
            localizedMovie = [[APMovie alloc] initWithTheMovieDBDictionary:dict];
        }

        //merging both medias
        [self _mergeLocalizedMedia:localizedMovie intoMedia:movie];
    }

    return movie;
}

- (APTVShow *)fetchTVShowMetadata:(APTVShow *)tvShowToFetch {

    //fetching with default language (en)
    NSInteger seriesId = [[[tvShowToFetch.episodeId componentsSeparatedByString:@"-"] objectAtIndex:0] integerValue];
    NSURL *tvShowURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceTvShowEpisode, seriesId, tvShowToFetch.seasonNumber, tvShowToFetch.episodeNumber, @"en"]];
    NSDictionary *dict = (id) [self performActionRequestToURL:tvShowURL usingCache:YES withMethod:@"GET" body:nil];
    APTVShow *tvShow = nil;
    if (dict != nil) {
        tvShow = [[APTVShow alloc] initWithTheMovieDBDictionary:dict];
    }
    NSURL *seriesURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceTvShow, seriesId, @"en"]];
    NSDictionary *seriesDict = (id) [self performActionRequestToURL:seriesURL usingCache:YES withMethod:@"GET" body:nil];
    if (seriesDict != nil) {
        [tvShow completeWithTheMovieDBSeriesDict:seriesDict];
    }

    //fetching with users preferred language if different
    NSString *preferredLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:kAPPreferencesPreferredLanguage];
    if (![preferredLanguage isEqualToString:@"en"]) {
        tvShowURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceTvShowEpisode, seriesId, tvShowToFetch.seasonNumber, tvShowToFetch.episodeNumber, preferredLanguage]];
        dict = (id) [self performActionRequestToURL:tvShowURL usingCache:YES withMethod:@"GET" body:nil];
        APTVShow *localizedTvShow = nil;
        if (dict != nil) {
            localizedTvShow = [[APTVShow alloc] initWithTheMovieDBDictionary:dict];
        }
        seriesURL = [NSURL URLWithString:[kTheMovieDBApiEndPoint stringByAppendingFormat:kTheMovieDBApiResourceTvShow, seriesId, preferredLanguage]];
        seriesDict = (id) [self performActionRequestToURL:seriesURL usingCache:YES withMethod:@"GET" body:nil];
        if (seriesDict != nil) {
            [localizedTvShow completeWithTheMovieDBSeriesDict:seriesDict];
        }
        //merging both medias
        [self _mergeLocalizedMedia:localizedTvShow intoMedia:tvShow];
    }

    tvShow.imageURL = tvShowToFetch.imageURL;



    return tvShow;
}

- (NSArray *)_findMediasUsingURL:(NSString *)stringUrl {

    NSURL *searchURL = [NSURL URLWithString:stringUrl];
    NSArray *resolvedMedias = (id) [self performActionRequestToURL:searchURL usingCache:YES withMethod:@"GET" body:nil];

    NSMutableArray *result = [[NSMutableArray alloc] init];
    if (resolvedMedias != nil && resolvedMedias.class != [NSNull class] && [resolvedMedias count] > 0) {
        for (NSDictionary *dict in resolvedMedias) {
            if (![dict isKindOfClass:[NSDictionary class]]) {
                continue;
            }
            APVideoMedia *video = nil;
            if ([[dict objectForKey:@"type"] isEqualToString:@"TVShow"]) {
                video = [[APTVShow alloc] init];
                ((APTVShow *) video).seriesTitle = [dict safeStringForKey:@"seriesTitle"];
                ((APTVShow *) video).episodeNumber = [dict safeIntegerForKey:@"episodeNumber"];
                ((APTVShow *) video).seasonNumber = [dict safeIntegerForKey:@"seasonNumber"];
                ((APTVShow *) video).episodeId = [dict safeStringForKey:@"id"];

            } else {
                video = [[APMovie alloc] init];
                ((APMovie *) video).imdbId = [dict objectForKey:@"imdbId"];
            }
            video.title = [dict objectForKey:@"title"];
            video.year = [[dict objectForKey:@"year"] class] != [NSNull class] ? [[dict objectForKey:@"year"] intValue] : 0;
            video.imageURL = [dict objectForKey:@"imageURL"] != nil && [[dict objectForKey:@"imageURL"] class] != [NSNull class] ? [NSURL URLWithString:[dict objectForKey:@"imageURL"]] : nil;


            [result addObject:video];
        }
    }

    return result;
}

- (void)_mergeLocalizedMedia:(APVideoMedia *)localizedMedia intoMedia:(APVideoMedia *)media {

    if (localizedMedia.imageURL != nil) {
        media.imageURL = localizedMedia.imageURL;
    }
    if (localizedMedia.genre != nil) {
        media.genre = localizedMedia.genre;
    }
    if (localizedMedia.title != nil) {
        media.title = localizedMedia.title;
    }
    if (localizedMedia.synopsis != nil) {
        media.synopsis = localizedMedia.synopsis;
    }
    if ([localizedMedia isKindOfClass:[APTVShow class]]) {
        ((APTVShow*)media).seriesTitle = ((APTVShow*)localizedMedia).seriesTitle;
    }
}

- (NSString *)_fileNameFromPath:(NSString *)filePath {
    return [[filePath lastPathComponent] stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale currentLocale]];
}

- (NSString *)_fileFolderFromPath:(NSString *)filePath {
    NSArray *pathComponents = [filePath pathComponents];
    if (pathComponents.count >= 2) {
        return [pathComponents objectAtIndex:pathComponents.count - 2];
    }
    return nil;
}

- (NSString *)_fileParentFolderFromPath:(NSString *)filePath {
    NSArray *pathComponents = [filePath pathComponents];
    if (pathComponents.count >= 3) {
        return [pathComponents objectAtIndex:pathComponents.count - 3];
    }
    return nil;
}

@end
