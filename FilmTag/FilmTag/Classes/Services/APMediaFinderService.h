//
//  APMediaFinderService.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-25.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APHTTPRequestServiceSupport.h"

@class APMovie;
@class APTVShow;

@interface APMediaFinderService : APHTTPRequestServiceSupport {

}

+ (id)sharedInstance;

- (NSArray *)findMediasForFilePath:(NSString *)filePath;

- (NSArray *)findMediasForKeywords:(NSString *)keywords;

- (APMovie *)fetchMovieMetadata:(APMovie *)movie;

- (APTVShow *)fetchTVShowMetadata:(APTVShow *)tvShowToFetch;

@end
