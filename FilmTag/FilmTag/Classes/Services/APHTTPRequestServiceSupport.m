//
//  HTTPServiceInterface.m
//  incidents
//
//  Created by Anthony Plourde on 11-11-18.
//  Copyright (c) 2012 Anthony Plourde.
//

#import "APHTTPRequestServiceSupport.h"
#import "SBJSON.h"
#import "XMLDictionary.h"

@implementation APHTTPRequestServiceSupport

- (void)cacheResponse:(NSDictionary *)response url:(NSURL *)url {

    if (response == nil) {
        return;
    }
    NSMutableDictionary *mutableResponse = [[NSMutableDictionary alloc] init];
    [mutableResponse setObject:response forKey:@"response"];
    [mutableResponse setObject:[[NSDate date] description] forKey:@"requestDate"];
    [_cache setObject:mutableResponse forKey:[url absoluteString]];
}

- (id)responseFromCache:(NSURL *)url {

    if (_cache == nil) {
        _cache = [[NSMutableDictionary alloc] init];
        return nil;
    }

    NSDictionary *cachedResponse = [_cache objectForKey:[url absoluteString]];

    if (!cachedResponse)
    {
        return nil;
    }
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *todayComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *oldDate = [NSDate dateWithString:[cachedResponse objectForKey:@"requestDate"]];
    NSDateComponents *earlierComponents = [cal components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:oldDate];

    //checking if cache is too old or not
    BOOL useCache = todayComponents.day == earlierComponents.day &&
            todayComponents.month == earlierComponents.month &&
            todayComponents.year == earlierComponents.year;

    if (useCache) {
        return [cachedResponse objectForKey:@"response"];
    }
    return nil;
}

- (NSDictionary *)performActionRequestToURL:(NSURL *)url usingCache:(BOOL)usingCache withMethod:(NSString *)method body:(NSString *)body {

    if (usingCache) {
        NSDictionary *cachedResponse = [self responseFromCache:url];
        if (cachedResponse != nil) {
#ifdef DEBUG
            NSLog(@"using cache for : %@", url);
#endif
            return cachedResponse;
        }
    }
#ifdef DEBUG
    NSLog(@"NOT using cache for : %@", url);
#endif

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setTimeoutInterval:45];

    //setting up method, can be GET, POST, PUT, DELETE
    [request setHTTPMethod:method];

    //setting up URL to hit
    [request setURL:url];

    //setting up header and credentials if applies
    if ([_acceptHeader length] > 0)
        [request setValue:_acceptHeader forHTTPHeaderField:@"Accept"];
    if ([_contentTypeHeader length] > 0)
        [request setValue:_contentTypeHeader forHTTPHeaderField:@"Content-Type"];
    if ([_tokenKeyHeader length] > 0 && [_tokenValueHeader length] > 0)
        [request setValue:_tokenValueHeader forHTTPHeaderField:_tokenKeyHeader];

    //setting up body content
    if (body)
        [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];

    NSURLResponse *response;
    NSError *error;

    //performing request
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    [self _showErrorPopupIfNecessary:response error:error];

    //parsing response to nsdictionary
    NSDictionary *dict = nil;
    if ([responseString length] > 3) {
        if ([[responseString substringToIndex:1] isEqualToString:@"{"] || [[responseString substringToIndex:1] isEqualToString:@"["]) {
            SBJSON *jsonParser = [[SBJSON alloc] init];
            dict = [jsonParser objectWithString:responseString error:NULL];
        } else if ([[responseString substringToIndex:1] isEqualToString:@"<"]) {
            dict = [NSDictionary dictionaryWithXMLString:responseString];
        } else {
            dict = [[NSDictionary alloc] initWithObjectsAndKeys:responseString, @"data", nil];
        }
    }

    [self cacheResponse:dict url:url];

    return dict;
}

- (void)_showErrorPopupIfNecessary:(NSURLResponse *)response error:(NSError *)error {
    if (error != nil || response == nil || [(NSHTTPURLResponse *) response statusCode] != 200) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSAlert *alert = [NSAlert alertWithMessageText:@"Oh snap!\nThe FilmTag server is unavailable."
                            defaultButton:@"OK"
                          alternateButton:nil
                              otherButton:nil
                informativeTextWithFormat:@"Don't panic, the server will be back soon.\n"
                        "Please try again later or contact info@filmtagapp.com\n\n"
                        "The application will now quit."];
            [alert setAlertStyle:NSCriticalAlertStyle];
            [alert runModal];
            [NSApp terminate:self];
        });
    }
}

@end
