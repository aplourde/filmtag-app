//
//  APMediaProcessingService.h
//  FilmTag
//
//  Created by Anthony Plourde on 2013-05-25.
//  Copyright (c) 2013 Anthony Plourde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <MP4v2/MP4File.h>

@class APVideoMedia;

@protocol APMovieProcessingDelegate <NSObject>

- (void)progressChanged:(double)newProgress;

- (void)success;

- (void)error:(NSString *)errorMessage;

@end

@interface APMediaProcessingService : NSObject <MP4FileDelegate> {

    id <APMovieProcessingDelegate> _delegate;
}

- (void)processMediaAtPath:(NSString *)inputPath usingMediaInfo:(APVideoMedia *)videoMedia;

@property(nonatomic, retain) id <APMovieProcessingDelegate> delegate;

@end
