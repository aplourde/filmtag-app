//
//  HTTPServiceInterface.h
//  incidents
//
//  Created by Anthony Plourde on 11-11-18.
//  Copyright (c) 2012 Anthony Plourde.
//

#import <Foundation/Foundation.h>

@interface APHTTPRequestServiceSupport : NSObject {

    NSString *_acceptHeader;
    NSString *_contentTypeHeader;
    NSString *_tokenKeyHeader;
    NSString *_tokenValueHeader;
    NSMutableDictionary *_cache;
}
- (NSDictionary *)performActionRequestToURL:(NSURL *)url usingCache:(BOOL)usingCache withMethod:(NSString *)method body:(NSString *)body;

@end
